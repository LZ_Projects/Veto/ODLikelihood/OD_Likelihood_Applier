#include "CutsOD_Likelihood_Applier.h"
#include "ConfigSvc.h"

CutsOD_Likelihood_Applier::CutsOD_Likelihood_Applier(EventBase *eventBase,
                                                         ConfigSvc *config) {
  m_event = eventBase;
  m_conf = config;

  // Check if filtering events
  int check_for_baccarat_event_ids{
      m_conf->configFileVarMap["BACCARAT_Event_ID_Selection"]};
  if (check_for_baccarat_event_ids == 1 || check_for_baccarat_event_ids == -1) {
    g_raw_file_names = Load_Raw_Files();
    g_baccarat_event_ids = Load_BACCARAT_Event_IDs();
  }
}

CutsOD_Likelihood_Applier::~CutsOD_Likelihood_Applier() {}

bool CutsOD_Likelihood_Applier::OD_Likelihood_ApplierCutsOK() {
  // List of common cuts for this analysis into one cut
  return true;
}

bool CutsOD_Likelihood_Applier::Cut_Pulse_IDs_Phd_Coincidence(
    int coincidence, float pulseArea_phd) {

  int coincidence_cut{m_conf->configFileVarMap["Pulse_Coincidence"]};
  float phd_cut{m_conf->configFileVarMap["Pulse_Area"]};

  if (coincidence > coincidence_cut && pulseArea_phd > phd_cut)
    return true;
  else
    return false;
}

bool CutsOD_Likelihood_Applier::Check_Event_ID() {
  // Check if filtering events
  int check_for_baccarat_event_ids{
      m_conf->configFileVarMap["BACCARAT_Event_ID_Selection"]};
  if (check_for_baccarat_event_ids == 0) {
    // process events if not using truth or a different selection
    return true;
  }
  else
  {
    bool found_event{false};

    // search arrays
    // lets program slowly and badly first
    int this_bacc_event{(*m_event->m_TruthEvent)->baccEvent};
    std::string this_raw_file_name{(*m_event->m_eventHeader)->rawFileName};

    // Check if it's there
    // TODO use iterator
    int file_index{-1};
    int count{0};
    for (auto &file_name : g_raw_file_names) {
      if (file_name == this_raw_file_name) {
        file_index = count;
      }
      count++;
    }
    // std::vector<int>::iterator file_it = std::find(g_raw_file_names.begin(),
    // g_raw_file_names.end(), this_raw_file_name);

    if (file_index > -1) {

      // Now search baccarat_event_ids
      std::vector<int>::iterator baccarat_it =
          std::find(g_baccarat_event_ids[file_index].begin(),
                    g_baccarat_event_ids[file_index].end(), this_bacc_event);
      if (baccarat_it != g_baccarat_event_ids[file_index].end()) {
        found_event = true;
      }
    }
    if (check_for_baccarat_event_ids == 1) {
      // If event was found, then we process the event
      return found_event;
    }
    else if (check_for_baccarat_event_ids == -1) {
      // If the event was found, we ignore it
      return !found_event;
    }
    // If here then the file and or event are not of interest
    return false;
  }

  // if here something has gone wrong!
  //FATAL("BACCARAT_Event_ID_Selection given not valid!");
  exit(0);
}

std::vector<string> CutsOD_Likelihood_Applier::Load_Raw_Files() {

  std::string file_name{m_conf->configFileVarMapString["Raw_Names_File"]};
  std::ifstream in(file_name);
  std::string str;
  std::vector<string> raw_files_vector;
  while (std::getline(in, str)) {
    if (str.size() > 0)
      raw_files_vector.push_back(str);
  }
  in.close();
  return raw_files_vector;
}

std::vector<std::vector<int>>
CutsOD_Likelihood_Applier::Load_BACCARAT_Event_IDs() {

  std::string file_name{m_conf->configFileVarMapString["BACCARAT_Event_IDs_File"]};
  std::ifstream in(file_name);
  std::string str;
  std::vector<std::vector<int>> baccarat_event_id_vector;
  std::vector<int> line_values;
  while (std::getline(in, str)) {
    line_values.clear();
    if (str.size() > 0) {
      std::istringstream iss(str);
      for (std::string str; iss >> str;) {
        line_values.push_back(std::stoi(str));
      }
    }
    if (line_values.size() > 0) {
      baccarat_event_id_vector.push_back(line_values);
    }
  }
  in.close();
  return baccarat_event_id_vector;
}