#include "HistogramsOD_Likelihood_Applier.h"
#include "HistSvc.h"

HistogramsOD_Likelihood_Applier::HistogramsOD_Likelihood_Applier(
    HistSvc *hists) {
  m_hists = hists;
}

HistogramsOD_Likelihood_Applier::~HistogramsOD_Likelihood_Applier() {}


void HistogramsOD_Likelihood_Applier::Likelihood(std::vector<float> likelihoods) {

  float rolling_likelihood{0.0};
  float likelihood;
  for (int i=0; i < likelihoods.size(); ++i ) {
    likelihood = likelihoods[i];
        m_hists->BookFillHist("Likelihood_v" + std::to_string(i), 20., 0., 1.,
                              likelihood);
        if (i == 0) {
          rolling_likelihood = likelihood;
        }
        else {
          rolling_likelihood = rolling_likelihood * likelihood;
          m_hists->BookFillHist("Likelihood_v1_to_v" + std::to_string(i),
                                20., 0., 1.,
                                rolling_likelihood);
        }
  }
  return;
}


