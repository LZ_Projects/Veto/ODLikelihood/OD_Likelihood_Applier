#include "OD_Likelihood_Applier.h"
#include "Analysis.h"
#include "ConfigSvc.h"
#include "CutsBase.h"
#include "CutsOD_Likelihood_Applier.h"
#include "EventBase.h"
#include "HistSvc.h"
#include "HistogramsOD_Likelihood_Applier.h"
#include "Logger.h"
#include "SkimSvc.h"
#include "SparseSvc.h"

OD_Likelihood_Applier::OD_Likelihood_Applier() : Analysis() {
  // Setup config service so it can be used to get config variables
  m_conf = ConfigSvc::Instance();
  g_od_gain_channel = m_conf->configFileVarMapString["OD_Gain"];
  INFO("Using OD pulses from " + g_od_gain_channel + " Gain Channel");

  // Load ROOT branches
  m_event->IncludeBranch("eventHeader");
  // only load either low gain or high gain, not both as not needed
  if (g_od_gain_channel == "High") {
    m_event->IncludeBranch("pulsesODHG");
  } else if (g_od_gain_channel == "Low") {
    m_event->IncludeBranch("pulsesODLG");
  } else {
    FATAL("OD Gain requested is not valid");
    exit(0);
  }
  m_event->Initialize();

  // Setup logging
  logging::set_program_name("OD_Likelihood_Applier Analysis");

  // Setup the analysis specific cuts.
  m_cutsOD_Likelihood_Applier =
      new CutsOD_Likelihood_Applier(m_event, m_conf);

  // Setup the histograms
  m_histograms = new HistogramsOD_Likelihood_Applier(m_hists);

  // Load likelihood variables
  g_likelihood_histograms.push_back(std::make_pair("pulseArea_phd", Load_Likelihood_From_File(m_conf->configFileVarMapString["var1"])));
  g_likelihood_histograms.push_back(std::make_pair("coincidence", Load_Likelihood_From_File(m_conf->configFileVarMapString["var2"])));
  g_likelihood_histograms.push_back(std::make_pair("areaFractionTime75_ns_over_pulseArea_phd", Load_Likelihood_From_File(m_conf->configFileVarMapString["var3"])));
  g_likelihood_histograms.push_back(std::make_pair("pulseEndTime_ns-pulseStartTime_ns", Load_Likelihood_From_File(m_conf->configFileVarMapString["var4"])));

}

OD_Likelihood_Applier::~OD_Likelihood_Applier() {
  delete m_cutsOD_Likelihood_Applier;
  delete m_histograms;
}

void OD_Likelihood_Applier::Initialize() {
  INFO("Initializing OD_Likelihood_Applier Analysis");
}

void OD_Likelihood_Applier::Execute() {
  // Select the branch and begin the processing
  if (g_od_gain_channel == "High") {
    Process_Event((m_event->m_odHGPulses));
  } else if (g_od_gain_channel == "Low") {
    Process_Event((m_event->m_odLGPulses));
  }
}

void OD_Likelihood_Applier::Finalize() {
  INFO("Finalizing OD_Likelihood_Applier Analysis");
}

void OD_Likelihood_Applier::Process_Event(
    TTreeReaderValue<ODPulses> *event_pulses) {

  if (!m_cutsOD_Likelihood_Applier->Check_Event_ID())
    return;

  // Get the pulse IDs
  std::vector<int> pulse_ids_all{Get_OD_Pulse_IDs(event_pulses)};

  // Do no more if there are no pulses
  if (pulse_ids_all.size() < 1)
    return;
  g_OD_event_phd = Calculate_Pulse_Area(event_pulses, pulse_ids_all);

  // Cut Events
  std::vector<int> pulse_ids_cut{Cut_Pulse_IDs(event_pulses, pulse_ids_all)};

  // Do no more if there are no pulses
  if (pulse_ids_cut.size() < 1)
    return;

  // pulse_ids_all)};
  std::vector<int> pulse_ids_ordered_cut{
      Order_Pulses_By_Phd(event_pulses, pulse_ids_cut)};

  // Look at the largest pulse
  std::vector<std::pair<std::string, float>>  pulse_parameters_first{
      Get_Single_Pulse_Parameters_OD(event_pulses, pulse_ids_ordered_cut[0])};
  std::string base_path{
      "CutPulses_phd" +
      std::to_string(m_conf->configFileVarMap["Pulse_Area"]) +
      "_coincidence" +
      std::to_string(
          m_conf->configFileVarMap["Pulse_Coincidence"])};

  Apply_Likelihood(pulse_parameters_first, base_path);


}

std::vector<int> OD_Likelihood_Applier::Get_OD_Pulse_IDs(
    TTreeReaderValue<ODPulses> *event_pulses) {
  std::vector<int> pulse_ids;
  for (int i = 0; i < (*event_pulses)->nPulses; ++i) {
    pulse_ids.push_back(i);
  }
  return pulse_ids;
}

float OD_Likelihood_Applier::Calculate_Pulse_Area(
    TTreeReaderValue<ODPulses> *event_pulses, std::vector<int> pulse_ids) {
  float phd{0.};
  for (auto &&id : pulse_ids) {
    phd += (*event_pulses)->pulseArea_phd[id];
  }
  return phd;
}

std::vector<int> OD_Likelihood_Applier::Order_Pulses_By_Phd(
    TTreeReaderValue<ODPulses> *event_pulses, std::vector<int> pulse_ids) {
  std::vector<std::pair<int, float>> pulses_pair; // pulse id and pulse phd
  std::vector<int> sorted_pulses;
  // Make pair for sorting
  for (auto &&id : pulse_ids) {
    pulses_pair.push_back(
        std::make_pair(id, (*event_pulses)->pulseArea_phd[id]));
  }
  auto sort_by_phd = [](const std::pair<int, float> &lhs,
                        const std::pair<int, float> &rhs) {
    return lhs.second > rhs.second;
  };
  std::sort(pulses_pair.begin(), pulses_pair.end(), sort_by_phd);
  // Put back into vector
  for (int i = 0; i < pulse_ids.size(); ++i) {
    sorted_pulses.push_back(pulses_pair[i].first);
  }
  return sorted_pulses;
}

std::vector<int>
OD_Likelihood_Applier::Cut_Pulse_IDs(TTreeReaderValue<ODPulses> *event_pulses,
                                       std::vector<int> pulse_ids) {
  std::vector<int> pulse_ids_selected;
  int coincidence;
  float pulseArea_phd;
  for (auto &&id : pulse_ids) {
    coincidence = (*event_pulses)->coincidence[id];
    pulseArea_phd = (*event_pulses)->pulseArea_phd[id];
    if (m_cutsOD_Likelihood_Applier->Cut_Pulse_IDs_Phd_Coincidence(
            coincidence, pulseArea_phd)) {
      pulse_ids_selected.push_back(id);
    }
  }
  return pulse_ids_selected;
}


std::vector<std::pair<std::string, float>> OD_Likelihood_Applier::Get_Single_Pulse_Parameters_OD(
    TTreeReaderValue<ODPulses> *event_pulses, int pulse_id) {

  std::vector<std::pair<std::string, float>> pulse_parameters;

  float v1 = (*event_pulses)->areaFractionTime75_ns[pulse_id];
  float v2 = (*event_pulses)->pulseArea_phd[pulse_id];

  pulse_parameters.push_back(std::make_pair("pulseArea_phd", (*event_pulses)->pulseArea_phd[pulse_id]));
  pulse_parameters.push_back(std::make_pair("coincidence", (*event_pulses)->coincidence[pulse_id]));
  pulse_parameters.push_back(std::make_pair("areaFractionTime75_ns_over_pulseArea_phd", v1 / v2));
  pulse_parameters.push_back(std::make_pair("pulseEndTime_ns-pulseStartTime_ns", (*event_pulses)->pulseEndTime_ns[pulse_id] - (*event_pulses)->pulseStartTime_ns[pulse_id]));

  return pulse_parameters;
}


std::pair<std::vector<float>,std::vector<float>>
OD_Likelihood_Applier::Load_Likelihood_From_File(std::string file_name) {

  std::ifstream in(file_name);
  std::string str;
  std::vector<std::vector<int>> likelihood_histogram;
  std::vector<float> bins;
  std::vector<float> values;
  std::vector<float> line_values;
  while (std::getline(in, str)) {
    line_values.clear();
    if (str.size() > 0) {
      std::istringstream iss(str);
      for (std::string str; iss >> str;) {
        line_values.push_back(std::stof(str));
      }
    }
    bins.push_back(line_values[0]);
    values.push_back(line_values[1]);
  }
  in.close();
  return std::make_pair(bins, values);
}

void OD_Likelihood_Applier::Apply_Likelihood(std::vector<std::pair<std::string, float>> pulse_parameters,
                                             std::string base_path) {

  std::vector<float> likelihoods;
  for (int i=0; i < pulse_parameters.size(); ++i) {
    likelihoods.push_back(Get_Likelihood_Value(pulse_parameters[i]));
  }
  // Now fill histograms
  m_histograms->Likelihood(likelihoods);
  return;
}


float OD_Likelihood_Applier::Get_Likelihood_Value(std::pair<std::string, float> pulse_parameters) {

  int bin;
  for (int i=0; i < g_likelihood_histograms.size(); ++i) {
    if (pulse_parameters.first == g_likelihood_histograms[i].first) {
      auto upper = std::upper_bound(g_likelihood_histograms[i].second.first.begin(),
                                    g_likelihood_histograms[i].second.first.end(),
                                    pulse_parameters.second);
      bin = (std::distance(g_likelihood_histograms[i].second.first.begin(), upper) - 1);
      return  g_likelihood_histograms[i].second.second[bin];
    }
  }
  FATAL("bin not found!");
  exit(1);
}