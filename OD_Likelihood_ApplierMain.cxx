#include "ConfigSvc.h"
#include "OD_Likelihood_Applier.h"
#include <string>

int main(int argc, char *argv[]) {
  // Crate config svc to read from unique config file
  string alpacaTopDir = std::getenv("ALPACA_TOPDIR");
  string analysisName("OD_Likelihood_Applier");
  string configFile("config/OD_Likelihood_Applier.config");
  string configFileFullPath =
      alpacaTopDir + "/modules/" + analysisName + "/" + configFile;

  ConfigSvc *config = ConfigSvc::Instance(argc, argv, analysisName,
                                          alpacaTopDir, configFileFullPath);

  // Create analysis code and run
  Analysis *ana;
  if (config->configFileVarMap["whichAna"] == 0) {
    ana = new OD_Likelihood_Applier();
    ana->Run(config->FileList, config->OutName, config->FileListTruth);
  }

  // Clean up
  delete ana;
  delete config;

  return 0;
}
