#ifndef OD_Likelihood_Applier_H
#define OD_Likelihood_Applier_H

#include "Analysis.h"

#include "CutsOD_Likelihood_Applier.h"
#include "EventBase.h"
#include "HistogramsOD_Likelihood_Applier.h"

#include <TTreeReader.h>
#include <string>

class SkimSvc;

class OD_Likelihood_Applier : public Analysis {

public:
  /// Constructor. Set which ROOT branches to load.
  OD_Likelihood_Applier();
  /// Destructor
  ~OD_Likelihood_Applier();
  /// Called once before the event loop
  void Initialize();
  /// Called once per event
  void Execute();
  /// Called once after event loop
  void Finalize();

  /**
   * @brief Primary processing function dictating what processing happens.
   * Called by Execute once per event.
   * @param event_pulses either (*m_event->m_odLGPulses) or
   * (*m_event->m_odHGPulses)
   */
  void Process_Event(TTreeReaderValue<ODPulses> *event_pulses);

  /**
   * @brief Create a vector containing integers starting from 0 up to nPulses -
   * 1
   * @param event_pulses either (*m_event->m_odLGPulses) or
   * (*m_event->m_odHGPulses)
   * @return
   */
  std::vector<int> Get_OD_Pulse_IDs(TTreeReaderValue<ODPulses> *event_pulses);

  /**
   * @brief Calculate the phd (pulseArea_phd) from a selection of pulses in an
   * event
   * @param event_pulses either (*m_event->m_odLGPulses) or
   * (*m_event->m_odHGPulses)
   * @param pulse_ids
   * @return
   */
  float Calculate_Pulse_Area(TTreeReaderValue<ODPulses> *event_pulses,
                             std::vector<int> pulse_ids);

  /**
   * @brief Rearrange the pulse_ids passed
   * @param event_pulses either (*m_event->m_odLGPulses) or
   * (*m_event->m_odHGPulses)
   * @param pulse_ids
   * @return pulse_ids in order of pulseArea_phd, with the largest pulse first
   */
  std::vector<int> Order_Pulses_By_Phd(TTreeReaderValue<ODPulses> *event_pulses,
                                       std::vector<int> pulse_ids);

  /**
   * @brief Get the pulse ids which pass the selection cut.
   * Coincidence and pulseArea_phd
   * @param event_pulses either (*m_event->m_odLGPulses) or
   * (*m_event->m_odHGPulses)
   * @param pulse_ids
   * @return
   */
  std::vector<int> Cut_Pulse_IDs(TTreeReaderValue<ODPulses> *event_pulses,
                                 std::vector<int> pulse_ids);

  /**
   * @brief Get RQs for a single pulse in an event
   * @param event_pulses either (*m_event->m_odLGPulses) or
   * (*m_event->m_odHGPulses)
   * @param pulse_id
   * @return pulse parameters
   */
  std::vector<std::pair<std::string, float>>
  Get_Single_Pulse_Parameters_OD(TTreeReaderValue<ODPulses> *event_pulses,
                                 int pulse_id);

  std::pair<std::vector<float>, std::vector<float>> Load_Likelihood_From_File(std::string file_name);

  void Apply_Likelihood(std::vector<std::pair<std::string, float>> pulse_parameters,
  std::string base_path);

  float Get_Likelihood_Value(std::pair<std::string, float> pulse_parameters);

    protected:
  /// Give access to event and pulse cuts
  CutsOD_Likelihood_Applier *m_cutsOD_Likelihood_Applier;

  /// Give access to Histogram fillers
  HistogramsOD_Likelihood_Applier *m_histograms;

  /// Give access to parameters defined in the OD_Likelihood_Applier.config
  ConfigSvc *m_conf;

  /// Determine which channel to perform the analysis on
  std::string g_od_gain_channel;

  /// Total phd seen by all OD pulses in the event
  float g_OD_event_phd;

  /// Total phd seen by all OD pulses in the event which survive the pulse
  /// selection cut
  float g_OD_cut_event_phd;

  std::vector<std::pair<std::string,std::pair<std::vector<float>,std::vector<float>>>> g_likelihood_histograms;
};

#endif
