#ifndef CutsOD_Likelihood_Applier_H
#define CutsOD_Likelihood_Applier_H

#include "EventBase.h"

class CutsOD_Likelihood_Applier {

public:
  /// Constructor
  CutsOD_Likelihood_Applier(EventBase *eventBase, ConfigSvc *config);

  /// Destructor
  ~CutsOD_Likelihood_Applier();

  /// Pointless function, should really remove
  bool OD_Likelihood_ApplierCutsOK();

  /**
   * @brief Check if the passed coincidence and pulseArea_phd pass the defined
   * selection (which is defined in the config file)
   * @param coincidence
   * @param pulseArea_phd
   * @return true if the pulse passes, otherwise false
   */
  bool Cut_Pulse_IDs_Phd_Coincidence(int coincidence, float pulseArea_phd);

  /**
   * @brief Load from file the rawFileNames that are to be processed.
   * @return
   */
  std::vector<string> Load_Raw_Files();

  /**
   * @brief Load from file the BACCARAT event IDs for each file which
   * should be processed
   * @return
   */
  std::vector<std::vector<int>> Load_BACCARAT_Event_IDs();

  /**
   * @brief Check if the current event should be processed based upon
   * baccEvent_id and rawFileName
   * @return True if BACCARAT eventID and rawFileName are valid
   */
  bool Check_Event_ID();

private:
  /// Get access to the event RQs
  EventBase *m_event;
  /// Give access to parameters defined in the OD_Likelihood_Applier.config
  ConfigSvc *m_conf;

  /// rawFileNames RQs to process
  std::vector<std::string> g_raw_file_names;

  /// BACCARAT event IDs to process
  std::vector<std::vector<int>> g_baccarat_event_ids;
};

#endif
