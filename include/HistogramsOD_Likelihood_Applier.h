#ifndef HistogramsOD_Likelihood_Applier_H
#define HistogramsOD_Likelihood_Applier_H

#include "EventBase.h"
#include "HistSvc.h"

class HistogramsOD_Likelihood_Applier {

public:
  /// Constructor
  HistogramsOD_Likelihood_Applier(HistSvc *hists);
  /// Destructor
  ~HistogramsOD_Likelihood_Applier();

  /**
   * @brief Create a set of histograms for a single pulse
   * @param base_path path within ROOT file
   * @param pulse_parameters
   * @param event_phd
   */
  void Likelihood(std::vector<float>);

private:
  /// ALPACA histogram functions
  HistSvc *m_hists;

};

#endif
